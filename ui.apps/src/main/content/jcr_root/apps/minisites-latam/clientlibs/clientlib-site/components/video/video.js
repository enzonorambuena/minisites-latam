$('.btn-video-close').hide();
$('.yt-video').hide();

var playerInfoList = []
var curplayer = {};
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

$('.player_youtube').each(function(i, obj) {
    var height = $(obj).parent().parent().height() < 100 ? ($(obj).parent().parent().width()/16)*9 : $(obj).parent().parent().height();
    var video_list = { id : $(obj).attr("id"), 
						height: height, 
						width:$(obj).parent().parent().width(), 
						videoId: $(obj).attr("video-code") 
					}
    playerInfoList.push(video_list);
});

function onYouTubeIframeAPIReady() {
    if(typeof playerInfoList === 'undefined')
        return; 

    for(var i = 0; i < playerInfoList.length;i++) {
        curplayer[playerInfoList[i].id] = createPlayer(playerInfoList[i]);
    }   
}

function createPlayer(playerInfo) {
    return new YT.Player(playerInfo.id, {
        height: playerInfo.height,
        width: playerInfo.width,
        videoId: playerInfo.videoId
    });
}

$(document).on("click",".btn-video", function () {
   var video_code = $(this).parent().parent().parent().find( ".player_youtube").attr("video-code");
   var id_div = $(this).parent().parent().parent().find( ".player_youtube").attr("id");

   $(this).hide() // boton play
   $(this).parent().parent().parent().find( "#image" ).hide();
   $(this).parent().parent().parent().find( "#title" ).hide();
   $(this).parent().parent().parent().find( "#subtitle" ).hide();
   $(this).parent().parent().parent().find( "#author" ).hide();
   $(this).parent().parent().parent().find( "#photographer" ).hide();

   $(this).parent().parent().parent().find( ".player_youtube").show(); // muestra video
   $(this).parent().parent().parent().find( ".btn-video-close" ).show();

   curplayer[id_div].playVideo();
});

$(document).on("click",".btn-video-close", function () {
   var video_code = $(this).parent().find( ".player_youtube").attr("video-code");
   var id_div = $(this).parent().find( ".player_youtube").attr("id");

    $(this).hide();
	$(this).parent().find( ".player_youtube" ).hide();

    $(this).parent().find( "#image" ).show();
    $(this).parent().find( "#title" ).show();
    $(this).parent().find( "#subtitle" ).show();
    $(this).parent().find( "#author" ).show();
    $(this).parent().find( "#photographer" ).show();
    $(this).parent().find( ".btn-video" ).show();
	curplayer[id_div].stopVideo();
});