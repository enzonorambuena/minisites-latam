$(function() {
	var accessToken = '7180697721.1677ed0.370189bca4214e81956ac2f0e4839bba';
    $.getJSON('https://api.instagram.com/v1/users/self/media/recent/?access_token=' + accessToken + '&callback=?', function(insta) {
        $.each(insta.data, function(photos, src) {
            if (photos === 5) { return false; }
            $('<a href="' + src.link + '" class="post" target="_blank">' +
                '<div class="image" style="background:url(' + src.images.standard_resolution.url + ')"></div>' +
                '<ul>' +
                '<li><i class="fa fa-heart"></i> ' + src.likes.count + '</li>' +
                '<li><i class="fa fa-comment"></i> ' + src.comments.count + '</li>' +
                '</ul></a>').appendTo("#content-instagram-token");
        });
    });

    $.getJSON('https://www.instagram.com/explore/tags/latam/?__a=1', function (data, status) {
    for(var i = 0; i < 10; i++) {
        var $this = data.graphql.hashtag.edge_hashtag_to_media.edges[i].node;
        $('#content-instagram-hashtag').append('<a href="javascript:void(0)" class="post">' +
        '<div class="image"><img src="'+  $this.thumbnail_resources[1].src +'"></div>' +
        '<ul>' +
        '<li><i class="fa fa-heart"></i> ' + $this.edge_liked_by.count + '</li>' +
        '<li><i class="fa fa-comment"></i> ' + $this.edge_media_to_comment.count + '</li>' +
        '</ul></a>');
    	}
	});

});