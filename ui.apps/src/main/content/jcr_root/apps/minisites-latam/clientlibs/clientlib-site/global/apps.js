(function($){

  // Main navigation
  var topNavCampaign = $('.top-nav__campaign');
  var mainMenu = $('.main-navigation');
  var pageHero = $('.page-hero');
  var pageHeroHeight = pageHero.outerHeight();
  if (pageHero.length) {
    topNavCampaign.hide();
    mainMenu.css({
      'top': pageHeroHeight + 'px',
    });
  }

  function onScroll(){
    // Header on scroll
    var winScroll = $(this).scrollTop();

    if (mainMenu.length) {
      if ( window.matchMedia('(min-width: 992px)').matches ) {
        if( winScroll >= mainMenu.offset().top ){
          topNavCampaign.slideDown();
        } else {
          topNavCampaign.slideUp();
        }
      }
    }

    // Active link switching
    var scrollLink = $('.scrollTo > a');
    var scrollbarLocation = $(this).scrollTop();

    scrollLink.each(function() {
      var sectionOffset = $(this.hash).offset().top;
      if ( sectionOffset <= scrollbarLocation ) {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
      }
    });

    // Scroll up
    if( winScroll > $(this).height() ){
      $('.scroll-up').fadeIn();
    } else {
      $('.scroll-up').fadeOut(800);
    }

  }

  $(document.body).on('touchmove', onScroll); // for mobile
  $(window).on('scroll', onScroll);

  // Smotth navigation
  $('.page-hero__hash-link').click(function(e){
    e.preventDefault();
    var headerOffset = window.matchMedia('(min-width: 992px)').matches ? 112 : 42;

    var linkHref = $(this).attr('href');
    $('html, body').animate({
      scrollTop: $(linkHref).offset().top - headerOffset,
    }, 800);
  });

  // Navbar Flyout
  $('.navbar-button').click( function(e) {
    e.preventDefault();
    $(this).attr('aria-expanded', true);
    $(this).removeClass('collapsed');
    $('.navbar-flyout').removeClass('closed');
    $('.navbar-flyout__content').removeClass('closed');
    $('.navbar-flyout__content').attr('aria-expanded', true);
  });

  $('.navbar-flyout__head-button').click( function () {
    $('.navbar-flyout').addClass('closed');
    $('.navbar-flyout__content').addClass('closed');
    $('.navbar-flyout__content').attr('aria-expanded', false);
    $('.navbar-toggle').addClass('collapsed');
    $('.navbar-toggle').attr('aria-expanded', false);
  });

  $(".nav-link").on('click touch', function() {
        $('.navbar-flyout').addClass('closed');
        $('.navbar-flyout__content').addClass('closed');
        $('.navbar-flyout__content').attr('aria-expanded', false);
        $('.navbar-toggle').addClass('collapsed');
        $('.navbar-toggle').attr('aria-expanded', false);
    });

  $('.navbar-flyout__content__nav .dropdown-menu .dropdown > a').click( function (e) {
    e.preventDefault();
    e.stopPropagation();
    var itemWrapper = $(this).closest('ul.dropdown-menu');
    if (itemWrapper.hasClass('open')) {
      itemWrapper.removeClass('open');
      $(this).next().hide();
    } else {
      itemWrapper.addClass('open');
      $(this).next().show();
    }
  });
    
  // Social media
  $('.social-media__button').click(function() {
    $('.social-media__list').toggleClass('social-media__list--open');
  });

  // SVG
  $('img.svg').each(function() {
    var $img = $(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    $.get(imgURL, function(data) {
      //Get the SVG tag, ignore the rest
      var $svg = $(data).find('svg');

      //Add replaced image's ID to the new SVG
      if(typeof imgID !== 'undefined') {
          $svg = $svg.attr('id', imgID);
      }
      //Add replaced image's classes to the new SVG
      if(typeof imgClass !== 'undefined') {
          $svg = $svg.attr('class', imgClass+' replaced-svg');
      }

      //Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      //Check if the viewport is set, if the viewport is not set the SVG wont't scale.
      if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
          $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
      }

      //Replace image with new SVG
      $img.replaceWith($svg);

    }, 'xml');

  });

  // Fancybox

  // Gallery carousel
  $('.gallery-carousel__items').owlCarousel({
    center: true,
    stagePadding: 0,
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    responsive: {
      0:{ items: 1, stagePadding: 0, margin: 0, },
      768:{ items: 1, stagePadding: 15, margin: 15, },
      992:{ items: 1, items: 1, stagePadding: 100, margin: 0, },
      1200:{ items: 3 }
    }
  });

  $('.cards-section-vamos__items-wrapper').owlCarousel({
    center: false,
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    responsive: {
      0:{ items: 1 },
      768:{ items: 2, },
      992:{ items: 3, },
      1200:{ items: 3 }
    }
  });

  $('.cards-section__items-wrapper').owlCarousel({
    center: true,
    stagePadding: 0,
    loop: true,
    margin: 0,
    nav: false,
    dots: true,
    responsive: {
      0:{ items: 1, center: false },
      768:{items: 2, center: false, loop: true, margin: 0},
      992:{items: 3, loop: true, margin: 0},
      1200:{items: 3, loop: true, margin: 0}
    }
  });

  var attractionsCarousel = $('.attractions-section__carousel');

  if (attractionsCarousel.length && window.matchMedia('(max-width: 767px)').matches ) {
    attractionsCarousel.addClass('owl-carousel');

    attractionsCarousel.owlCarousel({
      center: true,
      items: 1,
      stagePadding: 15,
      loop: true,
      margin: 15,
      nav: false,
      dots: true,
    });

  }

})( jQuery );