"use strict";

use (function () {

    var mapMode = this.mapMode;
    var apiKey = this.apiKey;
	var lat = this.latitude;
    var lng = this.longitude;;
    var center = lat+","+lng;
    var zoom = this.zoom;
    var maptype = this.maptype;
    var placeId = this.placeId;
    var place;
    var parameters;

    if(placeId == null){
		placeId = " ";
    }

    place = placeId.replace(/\s+/g, '+');

    if(mapMode == 'place'){
		parameters = "https://www.google.com/maps/embed/v1/".concat(mapMode,
                     "?key=",apiKey,"&q=",place,"&maptype=",maptype,"&zoom=",zoom);
    }

    if(mapMode == 'view'){
		parameters = "https://www.google.com/maps/embed/v1/".concat(mapMode,
    				 "?key=",apiKey, "&center=",center,"&maptype=",maptype,"&zoom=",zoom);
    }

    return{
        srcUrl: parameters
    };

});
