"use strict";
use(function(){

    var market = "";
    var templatePath = pageProperties.get("cq:template");
    var countryName = "";
    var countryIni = "";
    var flag = "";
	var countries = ["es_ar", "pt_br", "es_cl", "es_co", "es_ec",
                     "es_py", "es_pe", "es_uy", "en_ca", "es_mx",
                     "en_us", "es_us", "pt_us", "de_de", "es_es",
                     "fr_fr", "it_it", "en_uk", "en_ue", "en_au",
                     "en_nz", "es_un", "en_un"];

	for (var i = 0; i < countries.length; i++) {

        if(templatePath.includes("landing")) {  

			if(request.requestURI.indexOf("/"+countries[i]) >= 0){
                market = countries[i];
                language = market.substring(0,2);

                if(market=="es_ar"){
					countryName = "Argentina";
                    countryIni = "AR";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/argentina.svg";
                    break;
                }
                if(market=="pt_br"){
					countryName = "Brasil";
                    countryIni = "BR";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/brasil.svg";
                    break;
                }
                if(market=="es_cl"){
					countryName = "Chile";
                    countryIni = "CL";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/chile.svg";
                    break;
                }
                if(market=="es_co"){
					countryName = "Colombia";
                    countryIni = "CO";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/colombia.svg";
                    break;
                }
                if(market=="es_ec"){
					countryName = "Ecuador";
                    countryIni = "EC";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/ecuador.svg";
                    break;
                }
                if(market=="es_py"){
					countryName = "Paraguay";
                    countryIni = "PY";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/paraguay.svg";
                    break;
                }
                if(market=="es_pe"){
					countryName = "Perú";
                    countryIni = "PE";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/peru.svg";
                    break;
                }
                if(market=="es_uy"){
					countryName = "Uruguay";
                    countryIni = "UY";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/uruguay.svg";
                    break;
                }
                if(market=="en_ca"){
					countryName = "Canada";
                    countryIni = "CA";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/canada.svg";
                    break;
                }
                if(market=="es_mx"){
					countryName = "México";
                    countryIni = "MX";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/mexico.svg";
                    break;
                }
                if(market=="en_us"){
					countryName = "United States";
                    countryIni = "EN-US";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    break;
                }
                if(market=="es_us"){
					countryName = "Estados Unidos";
                    countryIni = "ES-US";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    break;
                }
                if(market=="pt_us"){
					countryName = "Estados Unidos";
                    countryIni = "PT-US";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    break;
                }
                if(market=="de_de"){
					countryName = "Deutschland";
                    countryIni = "DE";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/germany.svg";
                    break;
                }
                if(market=="es_es"){
                    countryName = "España";
                    countryIni = "ES";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/spain.svg";
                    break;
                }
                if(market=="fr_fr"){
					countryName = "France";
                    countryIni = "FR";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/france.svg";
                    break;
                }
                if(market=="it_it"){
					countryName = "Italia";
                    countryIni = "IT";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/italy.svg";
                    break;
                }
                if(market=="en_uk"){
					countryName = "United Kingdom";
                    countryIni = "UK";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-kingdom.svg";
                    break;
                }
                if(market=="en_ue"){
					countryName = "Rest of Europe";
                    countryIni = "UE";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/rest.svg";
                    break;
                }
                if(market=="en_au"){
					countryName = "Australia";
                    countryIni = "AU";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/australia.svg";
                    break;
                }
                if(market=="en_nz"){
					countryName = "New Zealand";
                    countryIni = "NZ";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/new-zealand.svg";
                    break;
                }
                if(market=="es_un"){
					countryName = "Otros Países";
                    countryIni = "ES-UN";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/other.svg";
                    break;
                }
                if(market=="en_un"){
					countryName = "Other Countries";
                    countryIni = "EN-UN";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/other.svg";
                    break;
                }
            } else {
                countryName = "Chile";
                countryIni = "CL";
                flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/chile.svg";
            }

        } else{

            if(request.requestURI.indexOf("/"+countries[i]+"/") >= 0){
                market = countries[i];
                language = market.substring(0,2);

                if(market=="es_ar"){
					countryName = "Argentina";
                    countryIni = "AR";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/argentina.svg";
                    break;
                }
                if(market=="pt_br"){
					countryName = "Brasil";
                    countryIni = "BR";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/brasil.svg";
                    break;
                }
                if(market=="es_cl"){
					countryName = "Chile";
                    countryIni = "CL";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/chile.svg";
                    break;
                }
                if(market=="es_co"){
					countryName = "Colombia";
                    countryIni = "CO";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/colombia.svg";
                    break;
                }
                if(market=="es_ec"){
					countryName = "Ecuador";
                    countryIni = "EC";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/ecuador.svg";
                    break;
                }
                if(market=="es_py"){
					countryName = "Paraguay";
                    countryIni = "PY";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/paraguay.svg";
                    break;
                }
                if(market=="es_pe"){
					countryName = "Perú";
                    countryIni = "PE";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/peru.svg";
                    break;
                }
                if(market=="es_uy"){
					countryName = "Uruguay";
                    countryIni = "UY";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/uruguay.svg";
                    break;
                }
                if(market=="en_ca"){
					countryName = "Canada";
                    countryIni = "CA";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/canada.svg";
                    break;
                }
                if(market=="es_mx"){
					countryName = "México";
                    countryIni = "MX";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/mexico.svg";
                    break;
                }
                if(market=="en_us"){
					countryName = "United States";
                    countryIni = "EN-US";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    break;
                }
                if(market=="es_us"){
					countryName = "Estados Unidos";
                    countryIni = "ES-US";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    break;
                }
                if(market=="pt_us"){
					countryName = "Estados Unidos";
                    countryIni = "PT-US";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    break;
                }
                if(market=="de_de"){
					countryName = "Deutschland";
                    countryIni = "DE";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/germany.svg";
                    break;
                }
                if(market=="es_es"){
                    countryName = "España";
                    countryIni = "ES";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/spain.svg";
                    break;
                }
                if(market=="fr_fr"){
					countryName = "France";
                    countryIni = "FR";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/france.svg";
                    break;
                }
                if(market=="it_it"){
					countryName = "Italia";
                    countryIni = "IT";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/italy.svg";
                    break;
                }
                if(market=="en_uk"){
					countryName = "United Kingdom";
                    countryIni = "UK";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-kingdom.svg";
                    break;
                }
                if(market=="en_ue"){
					countryName = "Rest of Europe";
                    countryIni = "UE";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/rest.svg";
                    break;
                }
                if(market=="en_au"){
					countryName = "Australia";
                    countryIni = "AU";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/australia.svg";
                    break;
                }
                if(market=="en_nz"){
					countryName = "New Zealand";
                    countryIni = "NZ";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/new-zealand.svg";
                    break;
                }
                if(market=="es_un"){
					countryName = "Otros Países";
                    countryIni = "ES-UN";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/other.svg";
                    break;
                }
                if(market=="en_un"){
					countryName = "Other Countries";
                    countryIni = "EN-UN";
                    flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/other.svg";
                    break;
                }
                break;
            } else {
                countryName = "Chile";
                countryIni = "CL";
                flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/chile.svg";
            }
        }

        if(request.requestURI.contains("master-contenido")){

            if(request.requestURI.contains("portugues")){
                countryName = "Brasil";
                countryIni = "BR";
                flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/brasil.svg";
            } else if(request.requestURI.contains("english")){
                countryName = "United States";
                countryIni = "EN-US";
                flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
            } else if(request.requestURI.contains("espanol")){
                countryName = "Chile";
                countryIni = "CL";
                flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/chile.svg";
            } 

        } else {
            countryName = "Chile";
            countryIni = "CL";
            flag = "/etc.clientlibs/minisites-latam/clientlibs/commons/resources/images/flags/chile.svg";
        }
	}

    return{
        market: market, countryName: countryName, countryIni: countryIni, flag: flag
    };

});