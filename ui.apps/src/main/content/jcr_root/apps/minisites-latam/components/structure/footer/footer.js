"use strict";
use(function(){

    var facebook = "";
    var instagram = "";
    var twitter = "";
    var youtube = "";
    var market = "";
    var language = "";
    var templatePath = pageProperties.get("cq:template");
    var countries = ["es_ar", "pt_br", "es_cl", "es_co", "es_ec",
                     "es_py", "es_pe", "es_uy", "en_ca", "es_mx",
                     "en_us", "es_us", "pt_us", "de_de", "es_es",
                     "fr_fr", "it_it", "en_uk", "en_ue", "en_au",
                     "en_nz", "es_un", "en_un"];

    for (var i = 0; i < countries.length; i++) {

        if(templatePath.includes("landing")) {  

            if(request.requestURI.indexOf("/"+countries[i]) >= 0){
                market = countries[i];
                language = market.substring(0,2);

                if(market=="es_ar"){
                    facebook = "http://www.facebook.com/LATAMArgentina/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_ARG";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="pt_br"){
                    facebook = "https://www.facebook.com/LATAMBrasil/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_BRA";
                    youtube = "https://www.youtube.com/user/tam";
                    break;
                }
                if(market=="es_cl"){
                    facebook = "https://www.facebook.com/LATAMChile/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_CHI";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_co"){
                    facebook = "https://www.facebook.com/LATAMColombia/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_CO";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_ec"){
                    facebook = "https://www.facebook.com/LATAMEcuador/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_ECU";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_py"){
                    facebook = "https://www.facebook.com/LATAMParaguay/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_PAR";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_pe"){
                    facebook = "https://www.facebook.com/LATAMPeru/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_PER";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_uy"){
                    facebook = "https://www.facebook.com/LATAMUruguay";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_URU";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="en_ca"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_mx"){
                    facebook = "https://www.facebook.com/LATAMMexico/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_MEX";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="en_us"){
                    facebook = "https://www.facebook.com/LATAMUnitedStates";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlinesUS";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_us"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="pt_us"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/tam";
                    break;
                }
                if(market=="de_de"){
                    facebook = "https://www.facebook.com/LATAMDeutschland";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_DE";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_es"){
                    facebook = "https://www.facebook.com/LATAMEspana";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_ES";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="fr_fr"){
                    facebook = "https://www.facebook.com/LATAMFrance/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_FR";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="it_it"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_uk"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_UK";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_ue"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_au"){
                    facebook = "http://www.facebook.com/LATAMAustralia/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_AUS";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_nz"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_un"){
                    facebook = "https://www.youtube.com/user/lanairlines";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="en_un"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
            } else {
                facebook = "https://www.facebook.com/LATAMAirlines/";
                instagram = "https://www.instagram.com/latamairlines/";
                twitter = "https://twitter.com/LATAMAirlines";
                youtube = "https://www.youtube.com/user/LANAirlinesUSA";
            }
        } else{

            if(request.requestURI.indexOf("/"+countries[i]+"/") >= 0){
                market = countries[i];
                language = market.substring(0,2);

                if(market=="es_ar"){
                    facebook = "http://www.facebook.com/LATAMArgentina/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_ARG";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="pt_br"){
                    facebook = "https://www.facebook.com/LATAMBrasil/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_BRA";
                    youtube = "https://www.youtube.com/user/tam";
                    break;
                }
                if(market=="es_cl"){
                    facebook = "https://www.facebook.com/LATAMChile/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_CHI";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_co"){
                    facebook = "https://www.facebook.com/LATAMColombia/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_CO";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_ec"){
                    facebook = "https://www.facebook.com/LATAMEcuador/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_ECU";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_py"){
                    facebook = "https://www.facebook.com/LATAMParaguay/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_PAR";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_pe"){
                    facebook = "https://www.facebook.com/LATAMPeru/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_PER";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="es_uy"){
                    facebook = "https://www.facebook.com/LATAMUruguay";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_URU";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="en_ca"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_mx"){
                    facebook = "https://www.facebook.com/LATAMMexico/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_MEX";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="en_us"){
                    facebook = "https://www.facebook.com/LATAMUnitedStates";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlinesUS";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_us"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="pt_us"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/tam";
                    break;
                }
                if(market=="de_de"){
                    facebook = "https://www.facebook.com/LATAMDeutschland";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_DE";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_es"){
                    facebook = "https://www.facebook.com/LATAMEspana";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_ES";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="fr_fr"){
                    facebook = "https://www.facebook.com/LATAMFrance/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_FR";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="it_it"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_uk"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_UK";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_ue"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_au"){
                    facebook = "http://www.facebook.com/LATAMAustralia/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAM_AUS";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="en_nz"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                if(market=="es_un"){
                    facebook = "https://www.youtube.com/user/lanairlines";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/lanairlines";
                    break;
                }
                if(market=="en_un"){
                    facebook = "https://www.facebook.com/LATAMAirlines/";
                    instagram = "https://www.instagram.com/latamairlines/";
                    twitter = "https://twitter.com/LATAMAirlines";
                    youtube = "https://www.youtube.com/user/LANAirlinesUSA";
                    break;
                }
                break;
            } else {
                facebook = "https://www.facebook.com/LATAMAirlines/";
                instagram = "https://www.instagram.com/latamairlines/";
                twitter = "https://twitter.com/LATAMAirlines";
                youtube = "https://www.youtube.com/user/LANAirlinesUSA";
            }
        }
    }

    return{
        facebook: facebook, instagram: instagram, twitter: twitter, youtube: youtube
    };

});