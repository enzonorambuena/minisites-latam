(function() {

    var market = document.getElementsByName('market')[0].content;
    
    if(market){
	    window.latamDatalayerService.sendPageView({
	    	app: 'cms',
	      	page: window.location.href,
	      	home: market,
	      	experience: 'CMS'
	    })
	} else {
		console.warn("Pageview not send : market is not defined");
	}

}());