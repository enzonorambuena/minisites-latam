"use strict";
use(function(){

    var market = "";
    var marketConf= "";
    var templatePath = pageProperties.get("cq:template");
 	var currentLocation = request.requestURL;
    var countryName = "";
    var countryIni = "";
    var flag = "";
    var corporate = "";
    var corporateLink = "";
    var helpCenter = "";
    var helpCenterLink = "";
	var countries = ["es_ar", "pt_br", "es_cl", "es_co", "es_ec",
                     "es_py", "es_pe", "es_uy", "en_ca", "es_mx",
                     "en_us", "es_us", "de_de", "es_es",
                     "fr_fr", "it_it", "en_uk", "en_ue", "en_au",
                     "en_nz", "es_un", "en_un", "pt_us"];
    var countriesHeader = ["es-ar-header", "pt-br-header", "es-cl-header", "es-co-header", "es-ec-header",
                           "es-py-header", "es-pe-header", "es-uy-header", "en-ca-header", "es-mx-header",
                           "en-us-header", "es-us-header", "de-de-header", "es-es-header",
                           "fr-fr-header", "it-it-header", "en-uk-header", "en-ue-header", "en-au-header",
                           "en-nz-header", "es-un-header", "en-un-header", "pt-us-header"];

	for (var i = 0; i < countries.length; i++) {

        if(templatePath.includes("home-landing")) {  

			if(request.requestURI.indexOf("/"+countries[i]) >= 0){
                market = countries[i];
                language = market.substring(0,2);

                if(market=="es_ar"){
					countryName = "Argentina";
                    countryIni = "AR";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/argentina.svg";
					corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_ar/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="pt_br"){
					countryName = "Brasil";
                    countryIni = "BR";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/brasil.svg";
					corporate = "LATAM Corporate";
                    corporateLink = "https://www.latam.com/corporate/pt_br/";
					helpCenter = "Central de Ajuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/pt-br";
                    break;
                }
                if(market=="es_cl"){
					countryName = "Chile";
                    countryIni = "CL";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/chile.svg";
					corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_cl/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_co"){
					countryName = "Colombia";
                    countryIni = "CO";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/colombia.svg";
					corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_co/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_ec"){
					countryName = "Ecuador";
                    countryIni = "EC";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/ecuador.svg";
					corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_ec/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_py"){
					countryName = "Paraguay";
                    countryIni = "PY";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/paraguay.svg";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_pe"){
					countryName = "Perú";
                    countryIni = "PE";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/peru.svg";
					corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_pe/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_uy"){
					countryName = "Uruguay";
                    countryIni = "UY";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/uruguay.svg";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="en_ca"){
					countryName = "Canada";
                    countryIni = "CA";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/canada.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="es_mx"){
					countryName = "México";
                    countryIni = "MX";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/mexico.svg";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="en_us"){
					countryName = "United States";
                    countryIni = "EN-US";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="es_us"){
					countryName = "Estados Unidos";
                    countryIni = "ES-US";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="pt_us"){
					countryName = "Estados Unidos";
                    countryIni = "PT-US";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
					helpCenter = "Central de Ajuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/pt-br";
                    break;
                }
                if(market=="de_de"){
					countryName = "Deutschland";
                    countryIni = "DE";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/germany.svg";
					helpCenter = "Hilfe";
                    helpCenterLink = "https://helpdesk.latam.com/hc/de";
                    break;
                }
                if(market=="es_es"){
                    countryName = "España";
                    countryIni = "ES";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/spain.svg";
					helpCenter = "Central de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="fr_fr"){
					countryName = "France";
                    countryIni = "FR";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/france.svg";
					helpCenter = "Centre d'assistance";
                    helpCenterLink = "https://helpdesk.latam.com/hc/fr";
                    break;
                }
                if(market=="it_it"){
					countryName = "Italia";
                    countryIni = "IT";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/italy.svg";
					helpCenter = "Aiuto";
                    helpCenterLink = "https://helpdesk.latam.com/hc/it";
                    break;
                }
                if(market=="en_uk"){
					countryName = "United Kingdom";
                    countryIni = "UK";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-kingdom.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="en_ue"){
					countryName = "Rest of Europe";
                    countryIni = "UE";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/rest.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="en_au"){
					countryName = "Australia";
                    countryIni = "AU";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/australia.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="en_nz"){
					countryName = "New Zealand";
                    countryIni = "NZ";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/new-zealand.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="es_un"){
					countryName = "Otros Países";
                    countryIni = "ES-UN";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/other.svg";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="en_un"){
					countryName = "Other Countries";
                    countryIni = "EN-UN";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/other.svg";
					helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }

           } else {
                market = "es_cl";
                countryName = "Chile";
                countryIni = "CL";
                flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/chile.svg";
                corporate = "Corporate";
                corporateLink = "https://www.latam.com/corporate/es_cl/";
				helpCenter = "Centro de Ayuda";
                helpCenterLink = "https://helpdesk.latam.com/hc/es";

            }
        } else{

            if(request.requestURI.indexOf("/"+countries[i]+"/") >= 0){
                market = countries[i];
                language = market.substring(0,2);

                if(market=="es_ar"){
					countryName = "Argentina";
                    countryIni = "AR";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/argentina.svg";
                    corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_ar/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="pt_br"){
					countryName = "Brasil";
                    countryIni = "BR";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/brasil.svg";
                    corporate = "LATAM Corporate";
                    corporateLink = "https://www.latam.com/corporate/pt_br/";
					helpCenter = "Central de Ajuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/pt-br";
                    break;
                }
                if(market=="es_cl"){
					countryName = "Chile";
                    countryIni = "CL";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/chile.svg";
                    corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_cl/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_co"){
					countryName = "Colombia";
                    countryIni = "CO";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/colombia.svg";
                    corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_co/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_ec"){
					countryName = "Ecuador";
                    countryIni = "EC";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/ecuador.svg";
                    corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_ec/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_py"){
					countryName = "Paraguay";
                    countryIni = "PY";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/paraguay.svg";
                    helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_pe"){
					countryName = "Perú";
                    countryIni = "PE";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/peru.svg";
                    corporate = "Corporate";
                    corporateLink = "https://www.latam.com/corporate/es_pe/";
					helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="es_uy"){
					countryName = "Uruguay";
                    countryIni = "UY";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/uruguay.svg";
                    helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="en_ca"){
					countryName = "Canada";
                    countryIni = "CA";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/canada.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="es_mx"){
					countryName = "México";
                    countryIni = "MX";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/mexico.svg";
                    helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="en_us"){
					countryName = "United States";
                    countryIni = "EN-US";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="es_us"){
					countryName = "Estados Unidos";
                    countryIni = "ES-US";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="pt_us"){
					countryName = "Estados Unidos";
                    countryIni = "PT-US";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-states-of-america.svg";
                    helpCenter = "Central de Ajuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/pt-br";
                    break;
                }
                if(market=="de_de"){
					countryName = "Deutschland";
                    countryIni = "DE";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/germany.svg";
                    helpCenter = "Hilfe";
                    helpCenterLink = "https://helpdesk.latam.com/hc/de";
                    break;
                }
                if(market=="es_es"){
                    countryName = "España";
                    countryIni = "ES";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/spain.svg";
                    helpCenter = "Central de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="fr_fr"){
					countryName = "France";
                    countryIni = "FR";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/france.svg";
                    helpCenter = "Centre d'assistance";
                    helpCenterLink = "https://helpdesk.latam.com/hc/fr";
                    break;
                }
                if(market=="it_it"){
					countryName = "Italia";
                    countryIni = "IT";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/italy.svg";
                    helpCenter = "Aiuto";
                    helpCenterLink = "https://helpdesk.latam.com/hc/it";
                    break;
                }
                if(market=="en_uk"){
					countryName = "United Kingdom";
                    countryIni = "UK";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/united-kingdom.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="en_ue"){
					countryName = "Rest of Europe";
                    countryIni = "UE";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/rest.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="en_au"){
					countryName = "Australia";
                    countryIni = "AU";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/australia.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="en_nz"){
					countryName = "New Zealand";
                    countryIni = "NZ";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/new-zealand.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                if(market=="es_un"){
					countryName = "Otros Países";
                    countryIni = "ES-UN";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/other.svg";
                    helpCenter = "Centro de Ayuda";
                    helpCenterLink = "https://helpdesk.latam.com/hc/es";
                    break;
                }
                if(market=="en_un"){
					countryName = "Other Countries";
                    countryIni = "EN-UN";
                    flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/other.svg";
                    helpCenter = "Help Center";
                    helpCenterLink = "https://helpdesk.latam.com/hc/en-us";
                    break;
                }
                break;

            }else{

                market = "es_cl";
                countryName = "Chile";
                countryIni = "CL";
                flag = "/etc.clientlibs/vamos-latam/clientlibs/commons/resources/images/flags/chile.svg";
                corporate = "Corporate";
                corporateLink = "https://www.latam.com/corporate/es_cl/";
				helpCenter = "Centro de Ayuda";
                helpCenterLink = "https://helpdesk.latam.com/hc/es";
            }
        }
	}


    return{
        market: market, marketConf: marketConf, countryName: countryName, countryIni: countryIni, flag: flag, 
        helpCenter: helpCenter, helpCenterLink: helpCenterLink, corporate: corporate, corporateLink: corporateLink
    };

});
