"use strict";
use(function() {
    var showDate = "";

    var month = new Date(pageProperties.get("cq:lastModified"));
    var month1 = new Date(pageProperties.get("cq:lastReplicated"));
	month = month.toString().substring(4,7);
	month1 = month1.toString().substring(4,7);

	switch(month) {
    	  case "Jan":
    		  month="Enero";
    	    break;
    	  case "Feb":
    		  month="Febrero";
    	    break;
    	  case "Mar":
    		  month="Marzo";
    	    break;
    	  case "Apr":
    		  month="Abril";
    	    break;
    	  case "May":
    		  month="Mayo";
    	    break;
    	  case "Jun":
    		  month="Junio";
    	    break;
    	  case "Jul":
    		  month="Julio";
    	    break;
    	  case "Aug":
    		  month="Agosto";
    	    break;
    	  case "Sep":
    		  month="Septiembre";
    	    break;
    	  case "Oct":
    		  month="Octubre";
    	    break;
    	  case "Nov":
    		  month="Noviembre";
    	    break;
    	  case "Dec":
    		  month="Diciembre";
    	    break;
    }
    switch(month1) {
    	  case "Jan":
    		  month1="Enero";
    	    break;
    	  case "Feb":
    		  month1="Febrero";
    	    break;
    	  case "Mar":
    		  month1="Marzo";
    	    break;
    	  case "Apr":
    		  month1="Abril";
    	    break;
    	  case "May":
    		  month1="Mayo";
    	    break;
    	  case "Jun":
    		  month1="Junio";
    	    break;
    	  case "Jul":
    		  month1="Julio";
    	    break;
    	  case "Aug":
    		  month1="Agosto";
    	    break;
    	  case "Sep":
    		  month1="Septiembre";
    	    break;
    	  case "Oct":
    		  month1="Octubre";
    	    break;
    	  case "Nov":
    		  month1="Noviembre";
    	    break;
    	  case "Dec":
    		  month1="Diciembre";
    	    break;
    }

    return{
        showDate: showDate , month : month, month1 : month1
    };
});