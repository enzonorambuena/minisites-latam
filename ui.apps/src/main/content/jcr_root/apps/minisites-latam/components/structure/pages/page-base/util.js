"use strict";
use(function(){

	var market = "";
    var language = "";
    var templatePath = pageProperties.get("cq:template");
 	var currentLocation = String(request.requestURL);
    var template = String(this.template);

    var countries = ["es_ar", "pt_br", "es_cl", "es_co", "es_ec",
                     "es_py", "es_pe", "es_uy", "en_ca", "es_mx",
                     "en_us", "es_us", "de_de", "es_es",
                     "fr_fr", "it_it", "en_uk", "en_ue", "en_au",
                     "en_nz", "es_un", "en_un", "pt_us"];

    for (var i = 0; i < countries.length; i++) {
        if(request.requestURI.indexOf("/"+countries[i]) >= 0 || request.requestURI.indexOf("/"+countries[i]+"/") >= 0){
            market = countries[i];
            language = market.substring(0,2);
            if(language == "fr" || language == "de" ||  language == "it"){
                language = "en";
            }
            break;
        } else if(request.requestURI.contains("master-contenido")){

            if(request.requestURI.contains("portugues")){
                language = "pt";
                market = "pt_br";
            } else if(request.requestURI.contains("english")){
				language = "en";
                market = "en_us";
            } else if(request.requestURI.contains("espanol")){
				language = "es";
                market = "es_cl";
            } 
            
        } else {
            market = "es_cl";
            language = market.substring(0,2);
        }
	}

    return{
        market: market, language: language, location: currentLocation
    };

});
