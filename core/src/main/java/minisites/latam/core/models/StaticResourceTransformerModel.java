package minisites.latam.core.models;

import java.util.Optional;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;


import minisites.latam.core.services.StaticResourceLinkService;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StaticResourceTransformerModel {
	
	@Inject
	private SlingHttpServletRequest request;
	
	@Inject
	private StaticResourceLinkService staticResourceLinkService;
	
	@Inject
	private String targetURI;
	
	@Inject
	private String targetChildNode;
	
	@Inject
	private String targetProperty;
	
	public String getURIExternalized() {
		return StringUtils.isNotBlank(targetURI) ? staticResourceLinkService.addStaticResourcesDomain(targetURI) : null;
	}
	
	public String getChildNodePropertyExternalized() {
		return Optional.ofNullable(this.request.getResource().getChild(this.targetChildNode))
				 	.filter(cr -> cr.getValueMap().containsKey(this.targetProperty))
				 	.map(cr -> cr.getValueMap().get(this.targetProperty, String.class))
				 	.map(tpv -> staticResourceLinkService.addStaticResourcesDomain(tpv))
				 	.orElse(null);
	}

}