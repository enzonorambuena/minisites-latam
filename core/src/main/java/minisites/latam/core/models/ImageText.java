package minisites.latam.core.models;

public class ImageText {
	
	private String multiTitle;
	private String multiText;
	private String multiSubtitle;

	public String getMultiTitle() {
		return multiTitle;
	}

	public void setMultiTitle(String multiTitle) {
		this.multiTitle = multiTitle;
	}

	public String getMultiText() {
		return multiText;
	}

	public void setMultiText(String multiText) {
		this.multiText = multiText;
	}
	
	public String getMultiSubtitle() {
		return multiSubtitle;
	}

	public void setMultiSubtitle(String multiSubtitle) {
		this.multiSubtitle = multiSubtitle;
	}

}