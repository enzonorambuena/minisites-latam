package minisites.latam.core.access;

import java.util.ArrayList;
import java.util.List;   
import com.adobe.cq.sightly.WCMUsePojo;
import javax.jcr.Node;
import javax.jcr.NodeIterator; 
import minisites.latam.core.models.ImageText;

public class ImageTextAccess extends WCMUsePojo{

  private List<ImageText> multiItems = new ArrayList<ImageText>();

  @Override
  public void activate() throws Exception {

     Node currentNode = getResource().adaptTo(Node.class);
     
     if(currentNode.hasNode("contentOptions")){
    	 
	     Node itemsNode = currentNode.getNode("contentOptions");
	     NodeIterator ni =  itemsNode.getNodes();
	     
	     String multiTitle;
	     String multiText;
	     String multiSubtitle;
	
	     while (ni.hasNext()) {
	
	      ImageText multiItem = new ImageText();          
	      Node child = (Node)ni.nextNode();
	
	         
	         multiTitle = child.hasProperty("title") ? child.getProperty("title").getString(): ""; 
	         
	         multiText = child.hasProperty("text") ? child.getProperty("text").getString(): "";
	         
	         multiSubtitle = child.hasProperty("subtitle") ? child.getProperty("subtitle").getString(): "";
	     
	         
	         multiItem.setMultiTitle(multiTitle);
	         multiItem.setMultiText(multiText);
	         multiItem.setMultiSubtitle(multiSubtitle);
	         
	         multiItems.add(multiItem);
	       
	     }  
     }
 } 

	 public List<ImageText> getMultiItems() {
		 return multiItems;
	 }
}