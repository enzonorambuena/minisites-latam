package minisites.latam.core.services;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

public interface StaticResourceLinkService {

    String STATIC_RESOURCES_DOMAIN_PROPERTY_NAME = "static.resources.domain";

    @ObjectClassDefinition(
            name = "LATAM Static Resource Link Service",
            description = "This configuration enables the domain for static resources. For example, 's.latamstatic.com'.")
    public static @interface Config {
        @AttributeDefinition(
            name = "Static resources domain",
            description = "Enable static resources domain",
            defaultValue = "false")
        boolean is_enabled();
    }

	/**
	 * True when OSGI property {@value #STATIC_RESOURCES_DOMAIN_PROPERTY_NAME} is
	 * configured. This depends on environment and mode configurations. 
	 * For example, config.publish.production
	 * empty is the default value
	 * 
	 * @return
	 */
	public Boolean isStaticResourcesDomainAvailable();
	
	/**
	 * It only resolves then the url starts with a single '/', not two. Otherwise,
	 * thr original URI will be returned.
	 * 
	 * It does not set any protocol name or special prefix. It just adds '//' at the
	 * begining of the domain name
	 * 
	 * @param targetURI
	 * @return
	 */
	public String addStaticResourcesDomain(String targetURI);

	/**
	 * It only resolves then the url starts with a single '/', not two. Otherwise,
	 * thr original URI will be returned
	 * 
	 * @param prefix
	 *            Domain name prefix (protocol + slashes). For example, https://
	 * @param targetURI
	 * @return
	 */
	public String addStaticResourcesDomain(String prefix, String targetURI);

}