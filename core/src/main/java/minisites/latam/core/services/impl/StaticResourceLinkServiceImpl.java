package minisites.latam.core.services.impl;

import javax.inject.Inject;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Source;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import minisites.latam.core.services.StaticResourceLinkService;

@Designate(ocd = StaticResourceLinkService.Config.class)
@Component(service = StaticResourceLinkService.class, immediate = false)
public class StaticResourceLinkServiceImpl implements StaticResourceLinkService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StaticResourceLinkServiceImpl.class);

    private boolean staticResourcesDomain;
    private String staticResourcesDomainObj;

    @Reference
    Externalizer externalizer;

    @Inject
    @Source("sling-object")
    private ResourceResolver resolver;

    @Activate
    @Modified
    public void activate(Config config) {
        LOGGER.debug(" activating/modifying with {}", config);

        try {
            this.staticResourcesDomainObj = externalizer.externalLink(resolver, "static-resources", "");
        } catch (IllegalArgumentException iae) {
            LOGGER.info("Static resources configuration is not set.");
            this.staticResourcesDomainObj = "";
        } finally {
            this.staticResourcesDomain = config.is_enabled();
        }
    }

    @Override
    public Boolean isStaticResourcesDomainAvailable() {
        return this.staticResourcesDomain;
    }

    @Override
    public String addStaticResourcesDomain(String targetURI) {
        return this.addStaticResourcesDomain("//", targetURI);
    }

    @Override
    public String addStaticResourcesDomain(String prefix, String targetURI) {
        if (!this.isStaticResourcesDomainAvailable() || targetURI.startsWith("//") || !targetURI.startsWith("/") || targetURI.length() < 2) {
            return targetURI;
        }else {
            String domain = this.staticResourcesDomainObj.startsWith("http") ? this.staticResourcesDomainObj : prefix + this.staticResourcesDomainObj;
            return domain + targetURI;
        }
    }

}